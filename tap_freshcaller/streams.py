"""Stream type classes for tap-freshcaller."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_freshcaller.client import freshcallerStream


class UsersStream(freshcallerStream):
    """Define custom stream."""

    name = "users"
    path = "v1/users"
    primary_keys = ["id"]
    records_jsonpath = "$.users[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("status", th.IntegerType),
        th.Property("preference", th.IntegerType),
        th.Property("mobile_app_preference", th.IntegerType),
        th.Property("last_call_time", th.DateTimeType),
        th.Property("last_seen_time", th.DateTimeType),
        th.Property("confirmed", th.BooleanType),
        th.Property("language", th.StringType),
        th.Property("time_zone", th.StringType),
        th.Property("deleted", th.BooleanType),
        th.Property("role", th.StringType),
        th.Property(
            "teams", th.ArrayType(th.ObjectType(th.Property("id", th.IntegerType)))
        ),
    ).to_dict()


class TeamsStream(freshcallerStream):
    """Define custom stream."""

    name = "teams"
    path = "v1/teams"
    primary_keys = ["id"]
    records_jsonpath = "$.teams[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("name", th.StringType),
        th.Property("description", th.StringType),
        th.Property("omni_channel", th.BooleanType),
        th.Property(
            "users", th.ArrayType(th.ObjectType(th.Property("id", th.IntegerType)))
        ),
    ).to_dict()


class CallsStream(freshcallerStream):
    """Define custom stream."""
    name = "calls"
    path = "v1/calls"
    primary_keys = ["id"]
    records_jsonpath = "$.calls[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("direction", th.StringType),
        th.Property("parent_call_id", th.IntegerType),
        th.Property("root_call_id", th.IntegerType),
        th.Property("phone_number_id", th.IntegerType),
        th.Property("phone_number", th.StringType),
        th.Property("assigned_agent_id", th.IntegerType),
        th.Property("assigned_team_id", th.IntegerType),
        th.Property("assigned_call_queue_id", th.IntegerType),
        th.Property("assigned_ivr_id", th.IntegerType),
        th.Property("call_notes", th.StringType),
        th.Property("bill_duration", th.NumberType),
        th.Property("bill_duration_unit", th.StringType),
        th.Property("created_time", th.DateTimeType),
        th.Property("updated_time", th.DateTimeType),
        th.Property(
            "recording",
            th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("url", th.StringType),
                th.Property("duration", th.IntegerType),
                th.Property("duration_unit", th.StringType),
            ),
        ),
        th.Property(
            "participants",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("call_id", th.IntegerType),
                    th.Property("caller_id", th.IntegerType),
                    th.Property("caller_number", th.StringType),
                    th.Property("participant_id", th.IntegerType),
                    th.Property("participant_type", th.StringType),
                    th.Property("connection_type", th.IntegerType),
                    th.Property("call_status", th.IntegerType),
                    th.Property("duration", th.IntegerType),
                    th.Property("duration_unit", th.StringType),
                    th.Property("cost", th.NumberType),
                    th.Property("cost_unit", th.StringType),
                    th.Property("enqueued_time", th.StringType),
                    th.Property("created_time", th.DateTimeType),
                    th.Property("updated_time", th.DateTimeType),
                )
            ),
        ),
    ).to_dict()


class CallMetricsStream(freshcallerStream):
    """Define custom stream."""
    name = "call_metrics"
    path = "v1/call_metrics"
    primary_keys = ["id"]
    records_jsonpath = "$.call_metrics[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("call_id", th.IntegerType),
        th.Property("ivr_time", th.IntegerType),
        th.Property("ivr_time_unit", th.StringType),
        th.Property("hold_duration", th.IntegerType),
        th.Property("hold_duration_unit", th.StringType),
        th.Property("call_work_time", th.IntegerType),
        th.Property("call_work_time_unit", th.StringType),
        th.Property("total_ringing_time", th.IntegerType),
        th.Property("total_ringing_time_unit", th.StringType),
        th.Property("talk_time", th.IntegerType),
        th.Property("talk_time_unit", th.StringType),
        th.Property("answering_speed", th.IntegerType),
        th.Property("answering_speed_unit", th.StringType),
        th.Property("recording_duration", th.IntegerType),
        th.Property("recording_duration_unit", th.StringType),
        th.Property("bill_duration", th.NumberType),
        th.Property("bill_duration_unit", th.StringType),
        th.Property("cost", th.NumberType),
        th.Property("cost_unit", th.StringType),
        th.Property("created_time", th.DateTimeType),
        th.Property("updated_time", th.DateTimeType)
    ).to_dict()