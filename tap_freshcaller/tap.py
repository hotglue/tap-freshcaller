"""freshcaller tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_freshcaller.streams import (
    UsersStream,
    TeamsStream,
    CallsStream,
    CallMetricsStream
)

STREAM_TYPES = [
    UsersStream,
    TeamsStream,
    CallsStream,
    CallMetricsStream
]


class Tapfreshcaller(Tap):
    """freshcaller tap class."""
    name = "tap-freshcaller"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True
        ),
        th.Property(
            "org_name",
            th.StringType,
            required=True
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync"
        )
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__=="__main__":
    Tapfreshcaller.cli()